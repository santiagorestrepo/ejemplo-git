const STATUS_DISPLAY = document.querySelector('.game-notification'),  //Creamos una variable llamada juego y llamamos a la clase de game-notification
  GAME_STATE = ["", "", "", "", "", "", "", "", ""], //Se crea una matriz con la finalidad de separar la ubicacion de la casillas del tablero  
  WINNINGS = [ //se crea con la finalidad de saber que secuencias son las ganadoras, definiendo cuales son las trazas que se consideran ganadoras
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ]

  WIN_MESSAGE = () => `El jugador ${currentPlayer} ha ganado!`, // funciones que van cambiando dependiendo de quien es el ganador 
  DRAW_MESSAGE = () => `El juego ha terminado en empate!`, // funcion que notifica que ha habido un empate 
  CURRENT_PLAYER_TURN = () => `Turno del jugador ${currentPlayer}`//Funcion que se encarga de definir de quien es el turno


// ==================== VARIABLES ==================== //
let gameActive = true, //variable voleana que se encarga de hacer la corroborar la continuidad del juego
  currentPlayer = "O" //Variable que cambia repecto al juego, permitiendo, dado que si las X juegas, y el juego continua, se le de la oportunidad al otro jugador de lanzar


// ==================== FUNCTIONS ==================== //

function main() { //funcion encargada de imprimir el turno del juagdor actual
  handleStatusDisplay(CURRENT_PLAYER_TURN())  
  listeners()
}

main()

function handleStatusDisplay(message) { //funcion encargada de enviar un mensaje
    STATUS_DISPLAY.innerHTML = message
}

function listeners() { //funcion encargada de manejar el click, definiendo el cual celda se dio y cual sino es el correspondiente
  document.querySelector('.game-container').addEventListener('click', handleCellClick) //lleva a detalle en que celda de jugo
  document.querySelector('.game-restart').addEventListener('click', handleRestartGame) //permite restabelcer el juego
}

function handleCellClick(clickedCellEvent /** Type Event **/) {
    const CLICK_EN_LA_CELDA = clickedCellEvent.target
    if (CLICK_EN_LA_CELDA.classList.contains('game-cell')) { //detecta a que celda le dimos click
      const CELDA_CON_CLICK = Array.from(CLICK_EN_LA_CELDA.parentNode.children).indexOf(CLICK_EN_LA_CELDA)
      if (ESTADO_DE_JUEGO[CELDA_CON_CLICK] !== '' || !CONTINUIDAD_DEL_JUEGO) {
        return false
      }
  
      handleCellPlayed(CLICK_EN_LA_CELDA, CELDA_CON_CLICK)
      handleResultValidation()
    }
}

function handleCellPlayed(CLICK_EN_LA_CELDA /** object HTML **/, CELDA_CON_CLICK) {
    ESTADO_DE_JUEGO[CELDA_CON_CLICK] = JUGADOR // Agrega en la posición correspondiente el valor ya sea "X" u "O" en el estado actual del juego
    CLICK_EN_LA_CELDA.innerHTML = JUGADOR // Agrega en el HTML el valor del jugador
}



function handleRestartGame() {
  gameActive = true;
  currentPlayer = "X";
  restartGameState();
  handleStatusDisplay(CURRENT_PLAYER_TURN());
  document.querySelectorAll('.game-cell').forEach(cell => cell.innerHTML = "");
}


  
